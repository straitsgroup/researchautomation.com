from django.db import models

# Create your models here.
class Charts(models.Model):

	name = models.CharField(max_length=200)
	class Meta:
		db_table = "charts"

		def __str__(self):
			return self.name
