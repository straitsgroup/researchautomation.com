from django.shortcuts import render
from django.views.generic import ListView, DetailView
from . models import Charts

# Create your views here.
# Create your views here.
class ChartDetailView(DetailView):
	model = Charts
	template_name = 'blog/blog_detail.html'