from django.urls import path
from django.conf.urls import url

# from . import views
from .views import (
	ChartDetailView
	)
urlpatterns = [
  	 # path('blogs', views.BlogListView.as_view(), name='blogs'),
  	 #url(r'^$', BlogListView, name='blogs'),

  	 #url(r'^(?P<slug>[\w-]+)/$', BlogDetailView.as_view(), name='blog_detail'), #Django Code Review #3 on joincfe.com/youtube/
     # path('blog/<slug:slug>/', views.BlogDetailView.as_view(), name='blog_detail'),
     path('<slug:slug>/', ChartDetailView.as_view(), name='chart'),

]
